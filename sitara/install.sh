#!/bin/bash

## Sitara production install scripts
##
## If you modify this file, don't forget to put it to
##   https://bitbucket.org/maximrtls/public-scripts/src/sitara/install.sh
##
##
## Preparation
##
##  1. Publish binaries using script ./publish.sh
##  2. On beaglebone run $ sudo sudo apt-get install -y curl
##  2+. If previous command didn't work $ sudo /usr/sbin/ntpdate pool.ntp.org && sudo apt-get update && sudo apt-get install -y curl
##
## To install on beaglebone black, use following command on beaglebone
##
##   $ curl https://bitbucket.org/maximrtls/public-scripts/raw/master/sitara/install.sh?RANDOM_STRING | bash
##
##   If above command doesn't work, try
##   $ curl -k https://bitbucket.org/maximrtls/public-scripts/raw/master/sitara/install.sh?RANDOM_STRING | bash
##
##
##
##
##


COMMIT=${COMMIT:-master}
INSTALL_PATH=${INSTALL_PATH:-/var/lib/maximrtls_sitara}
LOG_PATH=${LOG_PATH:-/var/log/maximrtls_sitara}

WORK_DIR=/tmp/sitara

# We need it to reset bitbucket cache
SALT=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)


echo
echo "Using public scripts commit: $COMMIT"
echo "Using INSTALL_PATH=$INSTALL_PATH"
echo "Using LOG_PATH=$LOG_PATH"

URL="https://bitbucket.org/maximrtls/public-scripts/raw/$COMMIT/sitara/sitara-build.tar.gz"

# echo "==> Setting correct time using ntpdate"
# sudo /usr/sbin/ntpdate pool.ntp.org


echo "==> Moving current sitara data to ~/sitara-backup"
sudo rm -rf ~/sitara-backup
mkdir ~/sitara-backup
cp $INSTALL_PATH/maximrtls_sitara ~/sitara-backup
cp -r $INSTALL_PATH/config ~/sitara-backup/config
cp -r $LOG_PATH ~/sitara-backup/logs


echo "==> Download"

sudo rm -rf $WORK_DIR
mkdir -p $WORK_DIR
cd $WORK_DIR
curl -L -k $URL?$SALT | tar xz


echo "==> Create directories"

sudo rm -rf $INSTALL_PATH
sudo rm -rf $LOG_PATH

sudo mkdir -p $INSTALL_PATH
sudo mkdir -p $LOG_PATH

echo "==> Placing components into proper places..."
sudo mv $WORK_DIR/maximrtls_sitara $INSTALL_PATH/maximrtls_sitara
sudo mv $WORK_DIR/config $INSTALL_PATH

echo
echo "==> Configuring system..."

chmod +x $WORK_DIR/*
sudo $WORK_DIR/add_sitara_to_etc_rc_local
sudo $WORK_DIR/add_permissions
sudo $WORK_DIR/set_sshd_usedns_no

echo "==> Creating uninstall script at ~/uninstall-sitara"
sudo mv $WORK_DIR/uninstall ~/uninstall-sitara

echo "==> Copying other scripts to $INSTALL_PATH"
sudo mv $WORK_DIR/version.txt $INSTALL_PATH/version.txt
sudo mv $WORK_DIR/run-production $INSTALL_PATH/run-production


echo "==> Cleaning up..."
cd /
rm -rf $WORK_DIR


echo
echo "Files are installed to"
echo "  - $INSTALL_PATH"
echo "  - $LOG_PATH"
echo
echo "You can uninstall sitara using script ~/uninstall-sitara"
echo
echo "Sitara is installed and will be run automatically when system start"
echo "It's configured in /etc/rc.local"
echo
echo "If you want to run without restarting, try"
echo "   $ cd $INSTALL_PATH"
echo "   $ sudo ./maximrtls_sitara"
echo
